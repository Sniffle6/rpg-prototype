﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcHealer : MonoBehaviour
{
    public ChatSystem chat;
    public TeamObject player;
    public GameObject chatPanel;
    bool canHeal = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            chatPanel.SetActive(true);
            chat.InitiateNextText("Press space to heal!");
            canHeal = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            chat.InitiateNextText("");
            chatPanel.SetActive(false);
            canHeal = false;
        }
    }
    private void Update()
    {
        if (canHeal)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                chat.InitiateNextText("...Healing......................... \nHealed!");
                for (int i = 0; i < player.Members.Count; i++)
                {
                    player.Members[i].currentHp = player.Members[i].maxHp.ModifiedValue;
                    if (player.Members[i].equipment.weapon.type != null)
                        player.Members[i].equipment.weapon.currentPP = player.Members[i].equipment.weapon.type.maxPP;
                    if (player.Members[i].equipment.armor.type != null)
                        player.Members[i].equipment.armor.currentPP = player.Members[i].equipment.armor.type.maxPP;
                    if (player.Members[i].equipment.shield.type != null)
                        player.Members[i].equipment.shield.currentPP = player.Members[i].equipment.shield.type.maxPP;
                    if (player.Members[i].equipment.trinket.type != null)
                        player.Members[i].equipment.trinket.currentPP = player.Members[i].equipment.trinket.type.maxPP;
                }
            }
        }
    }
}
