﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EquipmentScreen : MonoBehaviour
{
    public TeamObject player;
    public Inventory inventory;
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Level;
    public TextMeshProUGUI Exp;
    public TextMeshProUGUI Damage;
    public TextMeshProUGUI Defence;
    public Image Display;
    public Image Armor;
    public Image Weapon;
    public Image Shield;
    public Image Trinket;
    public Menu menu;
    public int selectedCharacter;
    public TextMeshProUGUI[] skillText;
    // Start is called before the first frame update
    void OnEnable()
    {
        selectedCharacter = player.SelectedMember;
        UpdateDisplay();
    }
    public void UpdateDisplay()
    {
        Name.text = player.Members[selectedCharacter].Name;
        Display.sprite = player.Members[selectedCharacter].type.EntityDisplay;
        if (player.Members[selectedCharacter].equipment.armor.type != null)
            Armor.sprite = player.Members[selectedCharacter].equipment.armor.type.display;
        else
            Armor.sprite = null;
        if (player.Members[selectedCharacter].equipment.weapon.type != null)
            Weapon.sprite = player.Members[selectedCharacter].equipment.weapon.type.display;
        else
            Weapon.sprite = null;
        if (player.Members[selectedCharacter].equipment.shield.type != null)
            Shield.sprite = player.Members[selectedCharacter].equipment.shield.type.display;
        else
            Shield.sprite = null;
        if (player.Members[selectedCharacter].equipment.trinket.type != null)
            Trinket.sprite = player.Members[selectedCharacter].equipment.trinket.type.display;
        else
            Trinket.sprite = null;
        for (int i = 0; i < skillText.Length; i++)
        {
            if (player.Members[selectedCharacter].skills[i] != null)
                skillText[i].text = string.Concat("Skill ",(i+1),":\n<size=28>", player.Members[selectedCharacter].skills[i].name);
            else
                skillText[i].text = string.Concat("Skill ", (i+1), ":\n<size=28>","No skill");
        }
        Level.text = string.Concat("Level:\n<size=28>", player.Members[selectedCharacter].level.Current);
        Exp.text = string.Concat("Experience:\n<size=28>", player.Members[selectedCharacter].level.Experience);
        Damage.text = string.Concat("Damage:\n<size=28>", player.Members[selectedCharacter].damage.ModifiedValue);
        Defence.text = string.Concat("Defence:\n<size=28>", player.Members[selectedCharacter].defence.ModifiedValue);
    }
    public void NextCharacter()
    {
        selectedCharacter += 1;
        if (selectedCharacter >= player.Members.Count)
            selectedCharacter = 0;
        UpdateDisplay();
    }
    public void PreviousCharacter()
    {
        selectedCharacter -= 1;
        if (selectedCharacter < 0)
            selectedCharacter = player.Members.Count - 1;
        UpdateDisplay();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (menu.currentSelection == 1)
            {
                var item = player.Members[selectedCharacter].equipment.weapon;
                if (item.type != null)
                {
                    inventory.container.items.Add(item);
                    player.Members[selectedCharacter].damage.RemoveModifier(item.type.stat);
                    player.Members[selectedCharacter].skills[0] = null;
                   // player.Members[selectedCharacter].skills.Remove(item.type.skill);
                }
                player.Members[selectedCharacter].equipment.weapon = new Items();
            }
            else if (menu.currentSelection == 4)
            {
                var item = player.Members[selectedCharacter].equipment.armor;
                if (item.type != null)
                {
                    inventory.container.items.Add(item);
                    player.Members[selectedCharacter].defence.RemoveModifier(item.type.stat);
                   // player.Members[selectedCharacter].skills.Remove(item.type.skill);
                    player.Members[selectedCharacter].skills[1] = null;
                }
                player.Members[selectedCharacter].equipment.armor = new Items();
            }
            else if (menu.currentSelection == 7)
            {
                var item = player.Members[selectedCharacter].equipment.shield;
                if (item.type != null)
                {
                    inventory.container.items.Add(item);
                    player.Members[selectedCharacter].defence.RemoveModifier(item.type.stat);
                   // player.Members[selectedCharacter].skills.Remove(item.type.skill);
                    player.Members[selectedCharacter].skills[2] = null;
                }
                player.Members[selectedCharacter].equipment.shield = new Items();
            }
            else if (menu.currentSelection == 2)
            {
                var item = player.Members[selectedCharacter].equipment.trinket;
                if (item.type != null)
                {
                    inventory.container.items.Add(item);
                    // player.Members[selectedCharacter].skills.Remove(item.type.skill);
                    player.Members[selectedCharacter].damage.RemoveModifier(item.type.stat);
                    player.Members[selectedCharacter].skills[3] = null;
                }
                player.Members[selectedCharacter].equipment.trinket = new Items();
            }
            UpdateDisplay();
        }
    }
}
