﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using ScriptableObjectArchitecture;
[System.Serializable]
public class Level
{
    [SerializeField]
    private int _currentLevel;
    [SerializeField]
    private int _experience;
    public LevelData data;
   // public IntGameEvent OnLevelUp;
   // public IntGameEvent OnXpGained;
    private const int MAX_EXP = 2147000000;

    public int Experience
    {
        get { return _experience; }
    }
    public int Current
    {
        get { return _currentLevel; }
    }
    public void Initiate()
    {
        _currentLevel = data.GetLevelForXP(Experience);
    }
    /// <summary>
        /// Add skill exp to current skill
        /// </summary>
        /// <param name="amount">The amount of xp to add</param>
        /// <returns>Returns true if you level up</returns>
    public bool AddXP(int amount)
    {
        if (amount + Experience < 0 || Experience > MAX_EXP)
        {
            if (Experience > MAX_EXP)
            {
                _experience = MAX_EXP;
            }
            return false;
        }
        int oldLevel = data.GetLevelForXP(Experience);
        _experience += amount;
       // OnXpGained.Raise(amount);
        return LevelUp(oldLevel);
    }
    private bool LevelUp(int oldLevel)
    {
        var newLevel = data.GetLevelForXP(Experience);
        if (oldLevel < newLevel)
        {
            if (_currentLevel < newLevel)
            {
                _currentLevel = newLevel;
               // if (OnLevelUp != null)
               //     OnLevelUp.Raise(this);
                return true;
            }
        }
        return false;
    }
}
