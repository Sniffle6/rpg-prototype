﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemObject : ScriptableObject
{
    public Sprite display;
    public StatModifier stat;
    public SkillObject skill;
    public int maxPP;
    public int ppCost;
    public int cost;
}
[System.Serializable]
public class StatModifier : IModifiers
{
    //public bool isUpdateable;
    public int value;
    public void AddValue(ref int v)
    {
        v += value;
    }

   // public IEnumerator OnUpdate()
   // {
   //     throw new System.NotImplementedException();
    //}
}