﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Current Menu", menuName = "Current Menu")]
public class MenuObject : ScriptableObject
{
    public int MenuInstanceId;
    public void SetInstanceID(GameObject obj)
    {
        MenuInstanceId = obj.GetInstanceID();
    }
    
}
