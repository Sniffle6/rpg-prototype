﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ScriptableObjectArchitecture;
public class CharacterSelection : MonoBehaviour
{
    public RectTransform parent;
    public GameObject characterPrefab;
    public TeamObject player;
    public int x_Start;
    public int x_Space;
    public int y_Start;
    public int y_Space;

    public int ColumnLength;
    public GenerateMenu menu;
    List<RectTransform> trans = new List<RectTransform>();
    public ChatSystem chat;
    public GameEvent returnToSelection;
    public FloatGameEvent select_Character;
    private void Start()
    {
    }
    private void OnEnable()
    {
        for (int i = 0; i < trans.Count; i++)
        {
            Destroy(trans[i].parent.gameObject);
        }
        trans.Clear();
        for (int i = 0; i < player.Members.Count; i++)
        {
            var position = new Vector3(parent.position.x + (x_Space * (i / ColumnLength)), parent.position.y + (-y_Space * (i % ColumnLength)));
            var panel = Instantiate(characterPrefab, position, Quaternion.identity, parent);
            panel.transform.GetChild(1).GetComponent<Image>().sprite = player.Members[i].type.EntityDisplay;
            panel.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = player.Members[i].Name;
            panel.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = string.Concat("Level: ", player.Members[i].level.Current);
            panel.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = string.Concat(player.Members[i].currentHp, "/", player.Members[i].maxHp.ModifiedValue);
            trans.Add(panel.transform.GetChild(0).GetComponent<RectTransform>());
        }

        menu.InitiateMenu(trans);
        ChangeColor(player.SelectedMember);
    }
    public void UpdateText()
    {
        chat.InitiateNextText("Select a Character...");
        for (int i = 0; i < trans.Count; i++)
        {
            var parentTrans = trans[i].parent.transform;
            parentTrans.GetChild(1).GetComponent<Image>().sprite = player.Members[i].type.EntityDisplay;
            parentTrans.GetChild(2).GetComponent<TextMeshProUGUI>().text = player.Members[i].Name;
            parentTrans.GetChild(3).GetComponent<TextMeshProUGUI>().text = string.Concat("Level: ", player.Members[i].level.Current);
            parentTrans.GetChild(4).GetComponent<TextMeshProUGUI>().text = string.Concat(player.Members[i].currentHp, "/", player.Members[i].maxHp.ModifiedValue);
        }
    }
    public void CanSelectCharacter(float choosen)
    {
        if (player.Members[(int)choosen].currentHp > 0)
        {
            if(player.SelectedMember != (int)choosen)
                select_Character.Raise(choosen);
            else
                chat.InitiateNextText("This character is already selected!");
        }
        else
            chat.InitiateNextText("This character is unconscious!");
    }
    public void LeaveCharacterPanel()
    {
        if(player.Members[player.SelectedMember].currentHp > 0)
        {
            returnToSelection.Raise();
        }
        else
        {
            print("Running");
            chat.InitiateNextText("You must pick a character with greater than 0 HP!");
        }
    }
    public void SendChat(float character)
    {
        chat.InitiateNextText(string.Concat(player.Members[(int)character].Name, " was set as leader!"));
    }
    public void ChangeColor(float bg)
    {
        for (int i = 0; i < trans.Count; i++)
        {
            if(i == bg)
            {
                trans[i].GetComponentInParent<Image>().color = new Color(0.75f,  0.75f, 0.75f, 1);
            }
            else
            {
                trans[i].GetComponentInParent<Image>().color = Color.white;
            }
        }
    }

}
