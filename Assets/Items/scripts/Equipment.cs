﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Equipment
{
    public Items weapon;
    public Items armor;
    public Items shield;
    public Items trinket;
}
