﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public static class SceneSystem 
{
    public static Vector3 playerPosition = new Vector3(-18.5f, -44.5f, 0.0f);
    public static int lastWorld;
    public static void LoadScene(int scene)
    {
        SceneManager.LoadSceneAsync(scene);
    }
    public static void LoadWorld()
    {
        SceneManager.LoadSceneAsync(0);
    }
    public static void LoadLastWorld()
    {
        SceneManager.LoadSceneAsync(lastWorld);
    }
    public static void LoadFight()
    {
        lastWorld = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadSceneAsync(1);
    }
}
