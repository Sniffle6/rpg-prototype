﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour
{
    EnemyInput input;
    public EncounterObject data;
    EntityInfo info;
    private void Start()
    {
        info = GetComponent<EntityInfo>();
        input = GetComponent<EnemyInput>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            bool chase = true;
            for (int i = 0; i < data.data.Count; i++)
            {
                if (info.EntityNumber == data.data[i].entityNumber && data.data[i].won == true)
                    chase = false;
            }
            if (!chase)
                return;
            GetComponentInChildren<Animator>().SetBool("Moving", true);
            input.direction = collision.transform.position - transform.position;
        }
    }
}
