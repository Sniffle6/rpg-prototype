﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum MenuNames
{
    Characters
}
public class MenuManager : MonoBehaviour
{
    public MenuObject data;
    public GameObject main_Menu;
    public TeamObject player;
    public bool canOpenMenu = true;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void SetCanOpenMenu(bool value)
    {
        canOpenMenu = value;
    }
    public void OpenMenu(MenuNames name)
    {

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && canOpenMenu)
        {
            if (!main_Menu.activeSelf)
            {
                player.pause = true;
                main_Menu.SetActive(true);
                data.SetInstanceID(main_Menu);
            }

        }
        if (!canOpenMenu)
        {
            main_Menu.SetActive(false);
        }
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
