﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Entity
{
    public string Name = "";
    public EntityObject type;
    public Level level;
    public ModifiableInt maxHp;
    public int currentHp;
    public ModifiableInt damage;
    public ModifiableInt defence;
    public Equipment equipment;
    public SkillObject[] skills = new SkillObject[4];
    string[] names = { "Achilleus", "Adolphus", "Albertus", "Aloysius", "Amadeus", "Ambrosius", "Aridius", "Arrius", "Atticus", "Augustus", "Aurelius", "Barnabas", "Beatus", "Brutus", "Cadmus", "Caius", "Caliban", "Cassius", "Cato", "Claudius", "Cletus", "Cornelius", "Cosmin", "Cronus", "Cyprian", "Cyrus", "Darius", "Decebal", "Emilian", "Erasmus", "Faustus", "Felix", "Flavius", "Florin", "Gaius", "Ignatius", "Julius", "Junius", "Justus", "Linus", "Livius", "Lucius", "Magnus", "Marcus", "Marius", "Maximus", "Mircea", "Nicabar", "Octavius", "Olivian", "Patrin", "Philo", "Pius", "Pontius", "Quintus", "Quirinus", "Remus", "Rufus", "Septimus", "Sergius", "Severus", "Sextus", "Silvanus", "Sirius", "Tarquin", "Thaddeus", "Theophilus", "Tiberius", "Titus", "Traian", "Tulio", "Ursinus", "Vitus", "Zephyrus" };
    public void LevelUp()
    {
        int temp = maxHp.BaseValue;
        maxHp.BaseValue = (int)System.Math.Ceiling(type.hp.stat + type.hp.growth * (level.Current - 1) * (0.7025 + 0.0175 * (level.Current - 1)));
        currentHp = (currentHp + (maxHp.BaseValue - temp));
        damage.BaseValue = (int)System.Math.Ceiling(type.damage.stat + type.damage.growth * (level.Current - 1) * (0.7025 + 0.0175 * (level.Current - 1)));
        defence.BaseValue = (int)System.Math.Ceiling(type.defence.stat + type.defence.growth * (level.Current - 1) * (0.7025 + 0.0175 * (level.Current - 1)));
    }
    public void Initiate()
    {
        if (Name == "")
            Name = names[Random.Range(0, names.Length)];
        for (int i = 0; i < skills.Length; i++)
        {
            skills[i] = null;
        }
        damage = new ModifiableInt(ValueModified)
        {
            BaseValue = (int)System.Math.Ceiling(type.damage.stat + type.damage.growth * (level.Current - 1) * (0.7025 + 0.0175 * (level.Current - 1)))
        };
        defence = new ModifiableInt(ValueModified)
        {
            BaseValue = (int)System.Math.Ceiling(type.defence.stat + type.defence.growth * (level.Current - 1) * (0.7025 + 0.0175 * (level.Current - 1)))
        };
        int temp = maxHp.BaseValue;
        maxHp = new ModifiableInt(ValueModified)
        {
            BaseValue = (int)System.Math.Ceiling(type.hp.stat + type.hp.growth * (level.Current - 1) * (0.7025 + 0.0175 * (level.Current - 1)))
        };
        currentHp = (currentHp + (maxHp.BaseValue - temp));

        if (equipment.weapon.type != null)
        {
            damage.AddModifier(equipment.weapon.type.stat);
            //skills.Add(equipment.weapon.type.skill);
            skills[0] = equipment.weapon.type.skill;
        }
        if (equipment.armor.type != null)
        {
            defence.AddModifier(equipment.armor.type.stat);
            // skills.Add(equipment.armor.type.skill);
            skills[1] = equipment.armor.type.skill;
        }
        if (equipment.shield.type != null)
        {
            defence.AddModifier(equipment.shield.type.stat);
            //skills.Add(equipment.shield.type.skill);
            skills[2] = equipment.shield.type.skill;
        }
        if (equipment.trinket.type != null)
        {
            damage.AddModifier(equipment.trinket.type.stat);
            // skills.Add(equipment.trinket.type.skill);
            skills[3] = equipment.trinket.type.skill;
        }
    }
    public void ValueModified() { }
}
