﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityMovement : MonoBehaviour
{
    public float speed;
    EntityInput input;
    Rigidbody2D rb;
    public EncounterObject obj;
    public bool useScenePosition;
    public TeamObject player;
    private void Start()
    {
        input = GetComponent<EntityInput>();
        rb = GetComponent<Rigidbody2D>();
        if (useScenePosition)
        {
            transform.position = SceneSystem.playerPosition;
        }
        else
        {
            if (obj != null)
                transform.position = obj.playerPosition;
        }
        bool allDead = true;
        if (player != null)
        {
            for (int i = 0; i < player.Members.Count; i++)
            {
                if (player.Members[i].currentHp > 0)
                    allDead = false;
            }
            if (allDead)
            {
                for (int i = 0; i < player.Members.Count; i++)
                {
                    player.Members[i].currentHp = player.Members[i].maxHp.ModifiedValue;
                    if (player.Members[i].equipment.weapon.type != null)
                        player.Members[i].equipment.weapon.currentPP = player.Members[i].equipment.weapon.type.maxPP;
                    if (player.Members[i].equipment.armor.type != null)
                        player.Members[i].equipment.armor.currentPP = player.Members[i].equipment.armor.type.maxPP;
                    if (player.Members[i].equipment.shield.type != null)
                        player.Members[i].equipment.shield.currentPP = player.Members[i].equipment.shield.type.maxPP;
                    if (player.Members[i].equipment.trinket.type != null)
                        player.Members[i].equipment.trinket.currentPP = player.Members[i].equipment.trinket.type.maxPP;
                }
                if (transform.position.x <= -10.0f && transform.position.y >= 44.5f)
                {
                    transform.position = new Vector3(-24.5f, 52.0f, 0.0f);
                }
                else
                {
                    transform.position = new Vector3(7.5f, -22.0f, 0.0f);
                }
            }
        }
        // rb.MovePosition()
    }
    public void ResetPosition()
    {
        transform.position = new Vector3(-15.0f, -44.0f, 0.0f);
    }
    private void FixedUpdate()
    {
        rb.MovePosition((Vector2)transform.position + input.direction * Time.deltaTime * speed);
    }
}
