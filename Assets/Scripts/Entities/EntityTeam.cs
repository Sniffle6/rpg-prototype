﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using ScriptableObjectArchitecture;
public class EntityTeam : MonoBehaviour
{
    [SerializeField]
    TeamObject team;
    [SerializeField]
    List<Entity> teamList;
    public int selectedMember;
    SpriteRenderer sRenderer;
    private void Start()
    {
        sRenderer = GetComponentInChildren<SpriteRenderer>();
        sRenderer.sprite = Selected().type.EntityDisplay;
        GetComponentInChildren<Animator>().runtimeAnimatorController = Selected().type.controller;
        if (team)
        {
            selectedMember = team.SelectedMember;
            for (int i = 0; i < team.Members.Count; i++)
            {
                team.Members[i].level.data = Resources.Load<LevelData>("Level Data");
                team.Members[i].level.Initiate();
                team.Members[i].Initiate();
                //team.Members[i]._level.OnLevelUp = Resources.Load<IntGameEvent>("OnLevelUp");
                //team.Members[i]._level.OnXpGained = Resources.Load<IntGameEvent>("OnXpGained");
            }
        }
        else
        {
            for (int i = 0; i < teamList.Count; i++)
            {
                teamList[i].level.data = Resources.Load<LevelData>("Level Data");
                teamList[i].level.Initiate();
                teamList[i].Initiate();
            }
        }
    }
    public List<Entity> GetTeam()
    {
        if (!team)
            return teamList;
        return team.Members;
    }
    private void Update()
    {
        if (tag != "Player")
            return;
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            int select = selectedMember += 1;
            ChooseCharacter(select);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            int select = selectedMember -= 1;
            ChooseCharacter(select);
        }
    }
    public void ChooseCharacter(float character)
    {
        if (!team)
            selectedMember = (int)character;
        else
            team.SelectedMember = (int)character;
        sRenderer.sprite = Selected().type.EntityDisplay;
        GetComponentInChildren<Animator>().runtimeAnimatorController = Selected().type.controller;
    }
    public Entity Selected()
    {
        if (!team)
            return teamList[selectedMember];
        return team.Members[team.SelectedMember];
    }
}
