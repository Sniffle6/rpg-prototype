﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : EntityInput
{
    public TeamObject player;
    void Update()
    {
        if (player.pause)
            return;
        if (Input.GetKey(KeyCode.W))
        {
            direction = direction.x == 0 ? new Vector2(direction.x, 1) : new Vector2(direction.x, 1) / 1.25f;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            direction = direction.x == 0 ? new Vector2(direction.x, -1) : new Vector2(direction.x, -1) / 1.25f;
        }
        else
        {
            direction = new Vector2(direction.x, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            direction = direction.y == 0 ? new Vector2(-1, direction.y) : new Vector2(-1, direction.y) / 1.25f;
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            direction = direction.y == 0 ? new Vector2(1, direction.y) : new Vector2(1, direction.y) / 1.25f;
            transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            direction = new Vector2(0, direction.y);
        }
    }
}
