﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Team", menuName = "Entity/New Team")]
public class TeamObject : ScriptableObject
{
    public bool pause;
    public int SelectedMember;
    public List<Entity> Members;
    public List<Items> Rewards = new List<Items>();
    public void Pause(bool value)
    {
        pause = value;
    }
}
