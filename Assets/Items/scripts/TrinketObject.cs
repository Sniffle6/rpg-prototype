﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New trinket", menuName = "Item/new trinket")]
public class TrinketObject : ItemObject
{
}
