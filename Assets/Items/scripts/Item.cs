﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Item : MonoBehaviour
{
    protected RectTransform rect;
    // public Modifier modifier;

    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }
    public abstract void Action();
}
