﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopulateGearSelection : MonoBehaviour
{
    public TeamObject player;
    public RectTransform parent;
    public GenerateMenu menu;
    public GameObject displayInfo;
    public GameObject equippedGearInfo;
    public Inventory inventory;
    public GameObject itemPrefab;
    public EquipmentScreen equip;
    public int y_Space;
    public List<RectTransform> trans = new List<RectTransform>();
    List<Items> items = new List<Items>();
    // Start is called before the first frame update
    public void DisplayGear(float type)
    {
        items.Clear();
        for (int i = 0; i < trans.Count; i++)
        {
            Destroy(trans[i].parent.gameObject);
        }
        trans.Clear();
        for (int i = 0; i < inventory.container.items.Count; i++)
        {
            if (inventory.container.items[i].type.GetType() != typeof(WeaponObject) && type == 1)
                continue;
            if (inventory.container.items[i].type.GetType() != typeof(TrinketObject) && type == 2)
                continue;
            if (inventory.container.items[i].type.GetType() != typeof(ArmorObject) && type == 4)
                continue;
            if (inventory.container.items[i].type.GetType() != typeof(ShieldObject) && type == 7)
                continue;
            //new Vector3(parent.position.x + (80 * (i / 10)), parent.position.y + (-y_Space * (i % 10)));
            var position = new Vector3(parent.position.x + (80 * (items.Count / 10)), parent.position.y + (-y_Space * (items.Count % 10)));
            var panel = Instantiate(itemPrefab, position, Quaternion.identity, parent);
            trans.Add(panel.transform.GetChild(2).GetComponent<RectTransform>());
           // panel.GetComponentInChildren<TextMeshProUGUI>().text = inventory.container.items[i].type.name;
            panel.GetComponentInChildren<Image>().sprite = inventory.container.items[i].type.display;
            panel.GetComponentInChildren<Image>().preserveAspect = true;
            items.Add(inventory.container.items[i]);
        }

        menu.InitiateMenu(trans);
        menu.SetSelectorMoved(UpdatePanel);
        //menu.SetSelectorMoved(SwapItem);
        UpdatePanel(0);
        UpdateEquppiedPanel((int)type);
    }
    public void UpdatePanel(int value)
    {
        if (trans.Count <= 0)
        {
            displayInfo.transform.GetChild(0).GetComponent<Image>().sprite =null;
            displayInfo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "N/A";
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Stat:\n<size=24>N/A";
            displayInfo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Skill:\n<size=24>N/A";
            return;
        }
        var item = trans[value];
        displayInfo.transform.GetChild(0).GetComponent<Image>().sprite = items[value].type.display;
        displayInfo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = items[value].type.name;
        if (items[value].type.GetType() == typeof(WeaponObject))
        {
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Damage:\n<size=24>", items[value].type.stat.value.ToString());
        }
        else if (items[value].type.GetType() == typeof(ShieldObject) || items[value].type.GetType() == typeof(ArmorObject))
        {
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Defence:\n<size=24>", items[value].type.stat.value.ToString());
        }
        else if (items[value].type.GetType() == typeof(TrinketObject))
        {
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Something:\n<size=24>", items[value].type.stat.value.ToString());
        }
        displayInfo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = string.Concat("Skill:\n<size=24>", items[value].type.skill.name);
    }
    public void UpdateEquppiedPanel(int value)
    {
        Items item = new Items();
        switch (value)
        {
            case 1:
                item = player.Members[equip.selectedCharacter].equipment.weapon;
                break;
            case 4:
                item = player.Members[equip.selectedCharacter].equipment.armor;
                break;
            case 7:
                item = player.Members[equip.selectedCharacter].equipment.shield;
                break;
            case 2:
                item = player.Members[equip.selectedCharacter].equipment.trinket;
                break;
            default:
                break;
        }
        if (item.type == null)
        {
            equippedGearInfo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "N/A";
            equippedGearInfo.transform.GetChild(0).GetComponent<Image>().sprite = null;
            equippedGearInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Stat:\n<size=24>N/A";
            equippedGearInfo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Skill:\n<size=24>N/A";
            return;
        }
        equippedGearInfo.transform.GetChild(0).GetComponent<Image>().sprite = item.type.display;
        equippedGearInfo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = item.type.name;
        if (item.type.GetType() == typeof(WeaponObject))
        {
            equippedGearInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Damage:\n<size=24>", item.type.stat.value.ToString());
        }
        else if (item.type.GetType() == typeof(ShieldObject) || item.type.GetType() == typeof(ArmorObject))
        {
            equippedGearInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Defence:\n<size=24>", item.type.stat.value.ToString());
        }
        else if (item.type.GetType() == typeof(TrinketObject))
        {
            equippedGearInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Damage:\n<size=24>", item.type.stat.value.ToString());
        }
        equippedGearInfo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = string.Concat("Skill:\n<size=24>", item.type.skill.name);
    }
    public void SwapItem(float value)
    {
        //find current selected item items[value]
        var item = items[(int)value];
        //remove from inventory items[value]
        inventory.container.items.Remove(item);
        //add items[value] to entity gear and remove current gear replace in inventory
        if (item.type.GetType() == typeof(WeaponObject))
        {
            var itemOnPlayer = player.Members[equip.selectedCharacter].equipment.weapon;
            if (itemOnPlayer.type != null)
            {
                inventory.container.items.Add(itemOnPlayer);
                player.Members[equip.selectedCharacter].damage.RemoveModifier(itemOnPlayer.type.stat);
                player.Members[equip.selectedCharacter].skills[0] = null;
            }
            player.Members[equip.selectedCharacter].equipment.weapon = item;
            player.Members[equip.selectedCharacter].damage.AddModifier(item.type.stat);
            player.Members[equip.selectedCharacter].skills[0] = item.type.skill;

        }
        else if (item.type.GetType() == typeof(ArmorObject))
        {
            var itemOnPlayer = player.Members[equip.selectedCharacter].equipment.armor;
            if (itemOnPlayer.type != null)
            {
                inventory.container.items.Add(itemOnPlayer);
                player.Members[equip.selectedCharacter].defence.RemoveModifier(itemOnPlayer.type.stat);
                player.Members[equip.selectedCharacter].skills[1] = null;
            }
            player.Members[equip.selectedCharacter].equipment.armor = item;
            player.Members[equip.selectedCharacter].defence.AddModifier(item.type.stat);
            player.Members[equip.selectedCharacter].skills[1] = item.type.skill;
        }
        else if (item.type.GetType() == typeof(ShieldObject))
        {
            var itemOnPlayer = player.Members[equip.selectedCharacter].equipment.shield;
            if (itemOnPlayer.type != null)
            {
                inventory.container.items.Add(itemOnPlayer);
                player.Members[equip.selectedCharacter].defence.RemoveModifier(itemOnPlayer.type.stat);
                player.Members[equip.selectedCharacter].skills[2] = null;
            }
            player.Members[equip.selectedCharacter].equipment.shield = item;
            player.Members[equip.selectedCharacter].defence.AddModifier(item.type.stat);
            player.Members[equip.selectedCharacter].skills[2] = item.type.skill;
        }
        else if (item.type.GetType() == typeof(TrinketObject))
        {
            var itemOnPlayer = player.Members[equip.selectedCharacter].equipment.trinket;
            if (itemOnPlayer.type != null)
            {
                inventory.container.items.Add(itemOnPlayer);
                player.Members[equip.selectedCharacter].damage.RemoveModifier(itemOnPlayer.type.stat);
                player.Members[equip.selectedCharacter].skills[3] = null;
            }
            player.Members[equip.selectedCharacter].equipment.trinket = item;
            player.Members[equip.selectedCharacter].damage.AddModifier(item.type.stat);
            player.Members[equip.selectedCharacter].skills[3] = item.type.skill;
        }
        //player.Members[equip.selectedCharacter].skills.Add(item.type.skill);
        menu.returnEvent.Raise();
        equip.UpdateDisplay();
    }
}
