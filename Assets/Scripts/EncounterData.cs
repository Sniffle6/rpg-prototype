﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class EncounterData
{
    public int entityNumber;
    public bool fought;
    public bool won;
    public bool recruited;
}