﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill", menuName ="Entity/New Skill")]
public class SkillObject : ScriptableObject
{
    public int damage;
    public GameObject vfx;
    public Vector3 scale;
}
