﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopulateItems : MonoBehaviour
{
    public RectTransform parent;
    public GenerateMenu menu;
    public GameObject displayInfo;
    public InventoryObject inventory;
    public GameObject itemPrefab;
    public InventoryObject playerInv;
    public int y_Space;
   public List<RectTransform> trans = new List<RectTransform>();
    // Start is called before the first frame update
    void OnEnable()
    {
        for (int i = 0; i < trans.Count; i++)
        {
            Destroy(trans[i].parent.gameObject);
        }
        trans.Clear();
        for (int i = 0; i < inventory.items.Count; i++)
        {
            //position = new Vector3(x_Start + (x_Space * (i % ColumnLength)), y_Start + (-y_Space * (i / ColumnLength)));
            var position = new Vector3(parent.position.x + (80 * (i / 10)), parent.position.y + (-y_Space * (i%10)));
            var panel = Instantiate(itemPrefab, position, Quaternion.identity, parent);
            trans.Add(panel.transform.GetChild(2).GetComponent<RectTransform>());
           // panel.GetComponentInChildren<TextMeshProUGUI>().text = inventory.items[i].type.name;
            panel.GetComponentInChildren<Image>().sprite = inventory.items[i].type.display;
            panel.GetComponentInChildren<Image>().preserveAspect = true;
        }

        menu.InitiateMenu(trans);
        menu.SetSelectorMoved(UpdatePanel);
        UpdatePanel(0);
        UpdateGold();
    }
    public void UpdateGold()
    {
        gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = playerInv.gold.ToString();
    }
    public void UpdateItems()
    {
        for (int i = 0; i < trans.Count; i++)
        {
            Destroy(trans[i].parent.gameObject);
        }
        trans.Clear();
        for (int i = 0; i < inventory.items.Count; i++)
        {
            var position = new Vector3(parent.position.x + (80 * (i / 10)), parent.position.y + (-y_Space * (i % 10)));
            var panel = Instantiate(itemPrefab, position, Quaternion.identity, parent);
            trans.Add(panel.transform.GetChild(2).GetComponent<RectTransform>());
           // panel.GetComponentInChildren<TextMeshProUGUI>().text = inventory.items[i].type.name;
            panel.GetComponentInChildren<Image>().sprite = inventory.items[i].type.display;
            panel.GetComponentInChildren<Image>().preserveAspect = true;
        }

        menu.InitiateMenu(trans);
        menu.SetSelectorMoved(UpdatePanel);
        UpdatePanel(0);
    }
    public void UpdatePanel(int value)
    {
        if (trans.Count <= 0)
            return;
        var item = trans[value];
        gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = playerInv.gold.ToString();
        displayInfo.transform.GetChild(0).GetComponent<Image>().sprite = inventory.items[value].type.display;
        displayInfo.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = inventory.items[value].type.name;
        displayInfo.transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = string.Concat("Worth: ", inventory.items[value].type.cost);
        displayInfo.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = string.Concat("Skill:\n<size=24>", inventory.items[value].type.skill.name);
        if (inventory.items[value].type.GetType() == typeof(WeaponObject))
        {
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Damage:\n<size=24>", inventory.items[value].type.stat.value.ToString());
        }
        else if (inventory.items[value].type.GetType() == typeof(ShieldObject) || inventory.items[value].GetType() == typeof(ArmorObject))
        {
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Defence:\n<size=24>", inventory.items[value].type.stat.value.ToString());
        }
        else if (inventory.items[value].type.GetType() == typeof(TrinketObject))
        {
            displayInfo.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = string.Concat("Damage:\n<size=24>", inventory.items[value].type.stat.value.ToString());
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
