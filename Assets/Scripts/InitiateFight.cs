﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitiateFight : MonoBehaviour
{
    public TeamObject playerTeam;
    public TeamObject enemyTeam;
    public EncounterObject entitesEncountered;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        bool found = false;
        if (collision.collider.tag == "Entity")
        {
            for (int i = 0; i < entitesEncountered.data.Count; i++)
            {
                if (collision.collider.GetComponent<EntityInfo>().EntityNumber == entitesEncountered.data[i].entityNumber)
                {
                    if (entitesEncountered.data[i].won)
                        return;
                    found = true;
                }
            }
            if (!found)
            {
                entitesEncountered.data.Add(new EncounterData
                {
                    entityNumber = collision.collider.GetComponent<EntityInfo>().EntityNumber,
                    fought = true
                });
            }
            enemyTeam.SelectedMember = collision.collider.GetComponent<EntityTeam>().selectedMember;
            enemyTeam.Members = collision.collider.GetComponent<EntityTeam>().GetTeam();
            enemyTeam.Rewards.Clear();
            for (int i = 0; i < collision.collider.GetComponent<EntityInfo>().rewards.Length; i++)
            {
                enemyTeam.Rewards.Add(collision.collider.GetComponent<EntityInfo>().rewards[i]);
            }
            playerTeam.pause = true;
            entitesEncountered.SelectedEnemy = collision.collider.GetComponent<EntityInfo>().EntityNumber;
            entitesEncountered.playerPosition = transform.position - (collision.collider.transform.position - transform.position);
            if (SceneManager.GetActiveScene().buildIndex == 0)
                SceneSystem.playerPosition = transform.position - (collision.collider.transform.position - transform.position);

            SceneSystem.LoadFight();

        }
    }
}

