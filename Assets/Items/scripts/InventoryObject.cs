﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Inventory", menuName = "New Inventory")]
public class InventoryObject : ScriptableObject
{
    public int gold;
    public List<Items> items = new List<Items>();
}
[System.Serializable]
public class Items
{
    public ItemObject type;
    public int currentPP;
}