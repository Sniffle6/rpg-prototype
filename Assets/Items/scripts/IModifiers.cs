﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[SerializeField]
public interface IModifiers
{
    void AddValue(ref int baseValue);
}
