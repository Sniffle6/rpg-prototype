﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ScriptableObjectArchitecture;

public class SetUpFight : MonoBehaviour
{
    public TeamObject player;
    public TeamObject enemy;
    public Image playerDisplay;
    public Image enemyDisplay;
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI enemyName;
    public TextMeshProUGUI playerLevel;
    public TextMeshProUGUI enemyLevel;
    public TextMeshProUGUI playerHp;
    public TextMeshProUGUI enemyHp;

    public TextMeshProUGUI skill1;
    public TextMeshProUGUI skill2;
    public TextMeshProUGUI skill3;
    public TextMeshProUGUI skill4;


    public TextMeshProUGUI skillDamage;
    public TextMeshProUGUI skillDurability;
    public TextMeshProUGUI skillDurabilityCost;

    public Menu skillmenu;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < player.Members.Count; i++)
        {
            player.Members[i].level.data = Resources.Load<LevelData>("Level Data");
            player.Members[i].level.Initiate();
            player.Members[i].LevelUp();
            //player.Members[i]._level.OnLevelUp = Resources.Load<IntGameEvent>("OnLevelUp");
            // player.Members[i]._level.OnXpGained = Resources.Load<IntGameEvent>("OnXpGained");
        }
        for (int i = 0; i < enemy.Members.Count; i++)
        {
            enemy.Members[i].level.data = Resources.Load<LevelData>("Level Data");
            enemy.Members[i].level.Initiate();
            enemy.Members[i].LevelUp();
            //player.Members[i]._level.OnLevelUp = Resources.Load<IntGameEvent>("OnLevelUp");
            // player.Members[i]._level.OnXpGained = Resources.Load<IntGameEvent>("OnXpGained");
        }
        playerDisplay.sprite = player.Members[player.SelectedMember].type.EntityDisplay;
        enemyDisplay.sprite = enemy.Members[enemy.SelectedMember].type.EntityDisplay;
        playerName.text = player.Members[player.SelectedMember].Name;
        playerLevel.text = string.Concat("Lv: ", player.Members[player.SelectedMember].level.Current.ToString());
       enemyName.text = enemy.Members[enemy.SelectedMember].Name;
        enemyLevel.text = string.Concat("Lv: ", enemy.Members[enemy.SelectedMember].level.Current.ToString());




    }

    // Update is called once per frame
    void Update()
    {
        enemyHp.text = string.Concat(enemy.Members[enemy.SelectedMember].currentHp.ToString(), "/", enemy.Members[enemy.SelectedMember].maxHp.ModifiedValue.ToString());
        playerHp.text = string.Concat(player.Members[player.SelectedMember].currentHp.ToString(), "/", player.Members[player.SelectedMember].maxHp.ModifiedValue.ToString());
        enemyLevel.text = string.Concat("Lv: ", enemy.Members[enemy.SelectedMember].level.Current.ToString());
        playerLevel.text = string.Concat("Lv: ", player.Members[player.SelectedMember].level.Current.ToString());

    }
    public void ChangeCharacter(float character)
    {
        player.SelectedMember = (int)character;
        playerDisplay.sprite = player.Members[player.SelectedMember].type.EntityDisplay;
        playerName.text = player.Members[player.SelectedMember].Name;
        playerLevel.text = string.Concat("Lv: ", player.Members[player.SelectedMember].level.Current.ToString());
    }
    public void ChangeEnemyCharacter(int character)
    {
        enemy.SelectedMember = (int)character;
        enemyDisplay.sprite = enemy.Members[enemy.SelectedMember].type.EntityDisplay;
        enemyName.text = enemy.Members[enemy.SelectedMember].Name;
        enemyLevel.text = string.Concat("Lv: ", enemy.Members[enemy.SelectedMember].level.Current.ToString());
    }
    public void UpdateSkills()
    {
        skill1.text = "No Skill";
        skill2.text = "No Skill";
        skill3.text = "No Skill";
        skill4.text = "No Skill";
        if (player.Members[player.SelectedMember].skills[0] != null)
            skill1.text = player.Members[player.SelectedMember].skills[0].name;
        if (player.Members[player.SelectedMember].skills[1] != null)
            skill2.text = player.Members[player.SelectedMember].skills[1].name;
        if (player.Members[player.SelectedMember].skills[2] != null)
            skill3.text = player.Members[player.SelectedMember].skills[2].name;
        if (player.Members[player.SelectedMember].skills[3] != null)
            skill4.text = player.Members[player.SelectedMember].skills[3].name;
        skillmenu.SetSelectorMoved(MoveAttackSelector);
        MoveAttackSelector(0);

    }
    public void MoveAttackSelector(int value)
    {
        Entity memeber = player.Members[player.SelectedMember];
        if (memeber.skills[value] == null)
        {
            skillDamage.text = "Damage:\n<size=20> N/A";
            skillDurability.text = "Durability:\n<size=20> N/A";
            skillDurabilityCost.text = "Durability Cost: <size=20> N/A";
        }
        else
        {
            skillDamage.text = string.Concat("Damage:\n<size=20>",(memeber.skills[value].damage + memeber.damage.ModifiedValue));
            switch (value)
            {
                case 0:
                    skillDurability.text = string.Concat("Durability:\n<size=20>", memeber.equipment.weapon.currentPP, "/", memeber.equipment.weapon.type.maxPP);
                    skillDurabilityCost.text = string.Concat("Durability Cost: <size=20>",memeber.equipment.weapon.type.ppCost);
                    break;
                case 1:
                    skillDurability.text = string.Concat("Durability:\n<size=20>",memeber.equipment.armor.currentPP, "/", memeber.equipment.armor.type.maxPP);
                    skillDurabilityCost.text = string.Concat("Durability Cost: <size=20>", memeber.equipment.armor.type.ppCost);
                    break;
                case 2:
                    skillDurability.text = string.Concat("Durability:\n<size=20>", memeber.equipment.shield.currentPP, "/", memeber.equipment.shield.type.maxPP);
                    skillDurabilityCost.text = string.Concat("Durability Cost: <size=20>", memeber.equipment.shield.type.ppCost);
                    break;
                case 3:
                    skillDurability.text = string.Concat("Durability:\n<size=20>", memeber.equipment.trinket.currentPP, "/", memeber.equipment.trinket.type.maxPP);
                    skillDurabilityCost.text = string.Concat("Durability Cost: <size=20>", memeber.equipment.trinket.type.ppCost);
                    break;
                default:
                    break;
            }
            //skillDamage.text = string.Concat("Durability:\n<size=20>", player.Members[player.SelectedMember].skills[value].dur);
        }
    }
}
