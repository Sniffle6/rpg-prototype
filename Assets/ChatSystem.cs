﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ChatSystem : MonoBehaviour
{
    public float speed;
    string TextToType;
    bool TypingText;
    private IEnumerator coroutine;
    public int StartDelay = 0;
    TextMeshProUGUI explination;

    void OnEnable()
    {
        explination = GetComponent<TextMeshProUGUI>();
        TextToType = explination.text;
        explination.text = "";
        Invoke(nameof(InvokeCorutine), StartDelay);
    }
    public void InitiateNextText(string text)
    {
        StopCorutine();
        TextToType = text;
        InvokeCorutine();
    }
    private void InvokeCorutine()
    {
        TypingText = true;
        coroutine = Typing();
        StartCoroutine(coroutine);
    }
    private void OnDisable()
    {
        explination.text = TextToType;
        TypingText = false;
        StopCoroutine(coroutine);
    }
    public void StopCorutine()
    {
        explination.text = TextToType;
        TypingText = false;
        if (coroutine != null)
            StopCoroutine(coroutine);
        StopAllCoroutines();
    }
    IEnumerator Typing()
    {
        int count = 0;
        int maxCount = TextToType.Length - 1;
        while (TypingText)
        {
            yield return new WaitForSeconds(speed);
            count++;
            if (count + 1 < TextToType.Length && char.IsWhiteSpace(TextToType[count]))
                count++;
            if (count > TextToType.Length)
            {
                TypingText = false;
                break;
            }
            explination.text = TextToType.Substring(0, count);
        }
    }
}
