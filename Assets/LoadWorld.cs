﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadWorld : MonoBehaviour
{
    public int sceneToLoad;
    public EncounterObject data;
    public GameObject player;
    public GameObject chatPanel;
    public ChatSystem chat;
    bool canUseAction = false;
    Vector2 positionToSet;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //chatPanel.SetActive(true);
        //canUseAction = true;
        // chat.InitiateNextText("Press space to enter door!");
        positionToSet = transform.position + (collision.transform.position - transform.position) * 2;
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            SceneSystem.playerPosition = positionToSet;
        }
        if (data)
        {
            data.playerPosition = positionToSet;
            //data.SetDirty();
        }

        StopAllCoroutines();
        StartCoroutine(SpawnWorld());
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (chatPanel.activeSelf)
            chat.InitiateNextText("");
        chatPanel.SetActive(false);
        canUseAction = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && canUseAction)
        {
            StopAllCoroutines();
            StartCoroutine(SpawnWorld());
        }
    }
    IEnumerator SpawnWorld()
    {
        yield return new WaitForSeconds(0.1f);
        SceneSystem.LoadScene(sceneToLoad);
    }
}
