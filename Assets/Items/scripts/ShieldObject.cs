﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New shield", menuName = "Item/new shield")]
public class ShieldObject : ItemObject
{
}
