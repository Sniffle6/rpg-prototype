﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecruitEntity : MonoBehaviour
{
    EntityTeam team;
    EntityInfo info;
    public TeamObject player;
    public EncounterObject encounter;
    public ChatSystem chat;
    public GameObject chatPanel;
    bool canRecruite = false;
    EncounterData encountered;
    // Start is called before the first frame update
    void Start()
    {
        info = GetComponent<EntityInfo>();
        team = GetComponent<EntityTeam>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (player.Members.Count >= 6)
            {
                canRecruite = false;
                chatPanel.SetActive(true);
                chat.InitiateNextText("Max team count reached!");
                return;
            }
            canRecruite = false;
            for (int i = 0; i < encounter.data.Count; i++)
            {
                if (encounter.data[i].entityNumber == info.EntityNumber)
                {
                    if (!encounter.data[i].recruited)
                    {
                        if (encounter.data[i].won)
                        {
                            encountered = encounter.data[i];
                            canRecruite = true;
                            chatPanel.SetActive(true);
                            chat.InitiateNextText("Press space to recruite member!");
                        }
                    }
                }
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (chatPanel.activeSelf)
            chat.InitiateNextText("");
        canRecruite = false;
        chatPanel.SetActive(false);
        encountered = null;
    }
    // Update is called once per frame
    void Update()
    {
        if (canRecruite)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (player.Members.Count >= 6)
                {
                    canRecruite = false;
                    chat.InitiateNextText("Max team count reached!");
                    return;
                }
                if (Random.Range(0.0f, 1.0f) > 0.75f)
                {
                    var entities = team.GetTeam();
                    var entityToRecruite = entities[Random.Range(0, entities.Count)];
                    player.Members.Add(entityToRecruite);
                    chat.InitiateNextText(string.Concat("Recruited ", entityToRecruite.type.name, " to your team!"));
                    encountered.recruited = true;
                    canRecruite = false;
                    StartCoroutine(ClosePanel());
                }
                else
                {
                    StartCoroutine(Failed());
                }
            }
        }
    }
    IEnumerator Failed()
    {
        if (chatPanel.activeSelf)
            chat.InitiateNextText("Failed to recruite a member");
        yield return new WaitForSeconds(1f);
        if (chatPanel.activeSelf)
            chat.InitiateNextText("Press space to recruite member!");
    }
    IEnumerator ClosePanel()
    {
        yield return new WaitForSeconds(2f);
        if (chatPanel.activeSelf)
            chat.InitiateNextText("");
        canRecruite = false;
        chatPanel.SetActive(false);
        encountered = null;

    }
}
