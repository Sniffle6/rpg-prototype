﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjectArchitecture;
public class Menu : MonoBehaviour
{
    public delegate void SelectorMoved(int value);
    public SelectorMoved selectorMoved;
    public bool StartMenu;
    public int currentSelection;
    public int columns;
    public List<MenuActions> actions = new List<MenuActions>();
    public RectTransform selector;
    public GameEvent returnEvent;
    public MenuObject selectedMenuID;
    public TurnSystem turn;
    // Start is called before the first frame update
    void Start()
    {
        if (StartMenu)
            selectedMenuID.MenuInstanceId = gameObject.GetInstanceID();
        selector.position = actions[0].selectionTransforms.position;
        currentSelection = 0;
    }
    public void InitiateMenu()
    {
        selector.position = actions[0].selectionTransforms.position;
        currentSelection = 0;
    }
    public void SetSelectorMoved(SelectorMoved value)
    {
        selectorMoved += value;
    }
    // Update is called once per frame
    void Update()
    {
        if (selectedMenuID.MenuInstanceId != gameObject.GetInstanceID() || (turn && !turn.playerTurn))
            return;
        if (actions.Count <= 0)
        {
            selector.localPosition = new Vector3(162, 247, 0);
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (actions[currentSelection].events != null)
                {
                    for (int i = 0; i < actions[currentSelection].events.Length; i++)
                    {
                        actions[currentSelection].events[i].Raise();
                    }
                }
                if (actions[currentSelection].floatEvent)
                    actions[currentSelection].floatEvent.Raise(currentSelection);


            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
            if (returnEvent)
                returnEvent.Raise();

        MoveSelection();
    }
    public void MoveSelection()
    {
        if (actions.Count <= 0)
            return;
        if (Input.GetKeyDown(KeyCode.D) && actions.Count > currentSelection + columns)
        {
            selector.position = actions[currentSelection + columns].selectionTransforms.position;
            currentSelection = currentSelection + columns;
            selectorMoved?.Invoke(currentSelection);
        }
        else if (Input.GetKeyDown(KeyCode.A) && currentSelection - columns >= 0)
        {
            selector.position = actions[currentSelection - columns].selectionTransforms.position;
            currentSelection = currentSelection - columns;
            selectorMoved?.Invoke(currentSelection);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            if (currentSelection > 0)
            {
                selector.position = actions[currentSelection - 1].selectionTransforms.position;
                currentSelection = currentSelection - 1;
                selectorMoved?.Invoke(currentSelection);
            }
            else
            {
                currentSelection = actions.Count - 1;
                selector.position = actions[currentSelection].selectionTransforms.position;
                selectorMoved?.Invoke(currentSelection);
            }
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            if (actions.Count > currentSelection + 1)
            {
                selector.position = actions[currentSelection + 1].selectionTransforms.position;
                currentSelection = currentSelection + 1;
                selectorMoved?.Invoke(currentSelection);
            }
            else
            {
                currentSelection = 0;
                selector.position = actions[currentSelection].selectionTransforms.position;
                selectorMoved?.Invoke(currentSelection);
            }
        }
    }
}
[System.Serializable]
public class MenuActions
{
    public RectTransform selectionTransforms;
    public FloatGameEvent floatEvent;
    public GameEventBase[] events;
}
