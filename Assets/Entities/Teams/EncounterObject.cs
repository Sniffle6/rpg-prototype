﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Encounter Data", menuName = "Encounter Data")]
public class EncounterObject : ScriptableObject
{
    public Vector3 playerPosition;
    public int SelectedEnemy;
    public List<EncounterData> data = new List<EncounterData>();
}