﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ScriptableObjectArchitecture;
public class GenerateMenu : Menu
{
    public bool setFloatEventOnStart;
    public FloatGameEvent gameEvent;
    private void Start()
    {
    }
    public void InitiateAutoGen()
    {
        if (!setFloatEventOnStart)
            return;
        for (int i = 0; i < actions.Count; i++)
        {
            actions[i].floatEvent = gameEvent;
        }
        selector.position = actions[0].selectionTransforms.position;
        currentSelection = 0;
    }
    public void InitiateMenu(List<RectTransform> trans)
    {
        actions.Clear();
        for (int i = 0; i < trans.Count; i++)
        {
            actions.Add(new MenuActions()
            {
                selectionTransforms = trans[i],
                floatEvent = gameEvent
            });
        }
        selectedMenuID.MenuInstanceId = gameObject.GetInstanceID();
        if (actions.Count > 0)
            selector.position = actions[0].selectionTransforms.position;

        currentSelection = 0;
    }

}
