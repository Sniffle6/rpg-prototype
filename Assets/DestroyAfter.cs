﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    public float delay;
    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(DestroyMe), delay);
    }
    public void DestroyMe()
    {
        Destroy(this.gameObject);
    }
    
}
