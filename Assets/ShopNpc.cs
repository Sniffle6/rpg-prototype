﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopNpc : MonoBehaviour
{
    public ChatSystem chat;
    public GameObject chatPanel;
    public GameObject shopCanvas;
    public MenuObject menu;
    public MenuManager menuManager;
    public TeamObject player;
    public InventoryObject shop;
    public InventoryObject playerInventory;
    public PopulateItems itemScreen;
    bool canOpenShop = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            chatPanel.SetActive(true);
            chat.InitiateNextText("Press space to open shop!");
            canOpenShop = true;
            menuManager.SetCanOpenMenu(false);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            chat.InitiateNextText("");
            chatPanel.SetActive(false);
            canOpenShop = false;
            menuManager.SetCanOpenMenu(true);
        }
    }
    private void Update()
    {
        if (canOpenShop)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                chat.InitiateNextText("Press space to buy item!");
                menuManager.SetCanOpenMenu(false);
                shopCanvas.SetActive(true);
                menu.SetInstanceID(shopCanvas.transform.GetChild(0).gameObject);
                player.pause = true;
            }
        }
    }
    public void BuyItem(float value)
    {
        var itemTobuy = shop.items[(int)value];
        if (playerInventory.gold >= itemTobuy.type.cost) {
            playerInventory.gold -= itemTobuy.type.cost;
            playerInventory.items.Add(itemTobuy);
            // shop.items.RemoveAt((int)value);
            itemScreen.UpdateGold();
            chat.InitiateNextText(string.Concat("Bought a ", itemTobuy.type.name, " for ", itemTobuy.type.cost));
        }
        else
        {
            chat.InitiateNextText(string.Concat("Can not afford a ", itemTobuy.type.name, ", you need ", itemTobuy.type.cost, " gold!"));
        }
    }
}
