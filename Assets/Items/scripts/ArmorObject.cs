﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New armor", menuName = "Item/new armor")]
public class ArmorObject : ItemObject
{
}
