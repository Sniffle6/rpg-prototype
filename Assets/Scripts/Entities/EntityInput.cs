﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EntityInput : MonoBehaviour
{
    public Vector2 direction;
}
