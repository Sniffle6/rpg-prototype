﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScriptableObjectArchitecture;
using TMPro;
public class TurnSystem : MonoBehaviour
{
    public bool playerTurn = true;
    public TeamObject player;
    public TeamObject enemy;
    public EncounterObject encounter;
    bool enemyCorutineRunning;
    bool playerCorutineRunning;
    bool lostCorutineRunning;
    bool sendEnemyCorutineRunning;
    bool changeCharacterCorutineRunning;
    public ChatSystem chat;
    public GameEvent returnToOption;
    public GameEvent select_character_option;
    public Image explinationUI;
    public TextMeshProUGUI turnLabel;
    public Coroutine playerCorutine;
    public Coroutine enemyCorutine;
    public SetUpFight setUp;
    public InventoryObject inventory;
    public void EndPlayerTurn()
    {
        playerTurn = false;
        if (playerCorutine != null)
        {
            StopCoroutine(playerCorutine);
            playerCorutineRunning = false;
        }
        if (enemyCorutineRunning)
            return;
        enemyCorutine = StartCoroutine(EnemyTurn());
        enemyCorutineRunning = true;
    }
    public void EndEnemyTurn()
    {
        if (enemyCorutine != null)
        {
            StopCoroutine(enemyCorutine);
            enemyCorutineRunning = false;
        }
        playerTurn = true;
        explinationUI.color = new Color(1, 1, 1);
        turnLabel.text = "Player Turn:";
        chat.InitiateNextText(string.Concat("Select an option..."));
    }
    public void ChangeCharacterEndTurn()
    {
        if (changeCharacterCorutineRunning)
            return;
        StartCoroutine(OnCharacterChange());
        changeCharacterCorutineRunning = true;
    }
    IEnumerator OnCharacterChange()
    {
        turnLabel.text = "Player Turn:";
        explinationUI.color = new Color(1, 1, 1);
        chat.InitiateNextText(string.Concat("Sent ", player.Members[player.SelectedMember].Name, " out to fight!"));
        playerTurn = false;
        yield return new WaitForSeconds(2f);
        changeCharacterCorutineRunning = false;
        EndPlayerTurn();
    }
    public void PlayerAttack(float skill)
    {
        print(playerTurn + "/" + playerCorutineRunning);
        if (playerTurn && !playerCorutineRunning)
        {
            print("hit");
            playerCorutine = StartCoroutine(PlayerTurn((int)skill));
            playerCorutineRunning = true;
        }
    }
    public bool CheckBattleWon()
    {
        if (enemy.Members[enemy.SelectedMember].currentHp <= 0)
        {
            if (AllCharactersDead(enemy))
            {
                StartCoroutine(Won());
                return true;
            }
            else
            {
                if (!sendEnemyCorutineRunning)
                {
                    sendEnemyCorutineRunning = true;
                    StartCoroutine(SendingEnemyOut());
                }
                return true;
            }
        }
        return false;
    }
    IEnumerator Won()
    {
        playerTurn = false;
        int levDiff =  enemy.Members[enemy.SelectedMember].level.Current - player.Members[player.SelectedMember].level.Current;
        int levDiffFinal = levDiff > 0 ? levDiff+2 : 1;
        int xpToAdd = Mathf.CeilToInt(levDiffFinal*0.75f) * 50 * Mathf.CeilToInt(player.Members[player.SelectedMember].level.Current * 0.5f);
        if (player.Members[player.SelectedMember].level.AddXP(xpToAdd))
        {
            chat.InitiateNextText(string.Concat("You defeated ", enemy.Members[enemy.SelectedMember].Name, " and gained ", xpToAdd," XP"));
            yield return new WaitForSeconds(2f);
            chat.InitiateNextText(string.Concat(player.Members[player.SelectedMember].Name, " gained a level!"));
            player.Members[player.SelectedMember].LevelUp();
            yield return new WaitForSeconds(2f);
            chat.InitiateNextText(string.Concat(player.Members[player.SelectedMember].Name, " is now level ", player.Members[player.SelectedMember].level.Current));
        }
        else
        {
            chat.InitiateNextText(string.Concat("You defeated ", enemy.Members[enemy.SelectedMember].Name, " and gained ", xpToAdd," XP"));
        }
        yield return new WaitForSeconds(2f);
        var item = enemy.Rewards[Random.Range(0, enemy.Rewards.Count)];
        inventory.items.Add(item);
        int goldTogain = Random.Range(0, (enemy.Members.Count * (enemy.Members[0].level.Current * 10) + 1000));
        inventory.gold += goldTogain;
        chat.InitiateNextText(string.Concat("You defeated all the enemies won the battle recieved a ", item.type.name," and ", goldTogain, " gold!"));
        yield return new WaitForSeconds(4f);
        player.pause = false;
        for (int i = 0; i < encounter.data.Count; i++)
        {
            if (encounter.data[i].entityNumber == encounter.SelectedEnemy)
            {
                encounter.data[i].won = true;
            }
        }
        SceneSystem.LoadLastWorld();
    }
    IEnumerator SendingEnemyOut()
    {
        if (player.Members[player.SelectedMember].level.AddXP(100))
        {
            chat.InitiateNextText(string.Concat("You defeated ", enemy.Members[enemy.SelectedMember].Name, " and gained 100 XP"));
            yield return new WaitForSeconds(2f);
            chat.InitiateNextText(string.Concat(player.Members[player.SelectedMember].Name, " gained a level!"));
            player.Members[player.SelectedMember].LevelUp();
            yield return new WaitForSeconds(2f);
            chat.InitiateNextText(string.Concat(player.Members[player.SelectedMember].Name, " is now level ", player.Members[player.SelectedMember].level.Current));
        }
        else
        {
            chat.InitiateNextText(string.Concat("You defeated ", enemy.Members[enemy.SelectedMember].Name, " and gained 100 XP"));
        }
        yield return new WaitForSeconds(2f);
        int select = -1;
        for (int i = 0; i < enemy.Members.Count; i++)
        {
            if (enemy.Members[i].currentHp > 0)
                select = i;
        }
        setUp.ChangeEnemyCharacter(select);
        explinationUI.color = new Color(1, 0.8f, 0.8f);
        turnLabel.text = "Enemy Turn:";
        chat.InitiateNextText("Enemy sent out " + enemy.Members[select].Name);
        yield return new WaitForSeconds(2f);
        EndEnemyTurn();
        sendEnemyCorutineRunning = false;
    }
    public bool CheckBattleLost()
    {
        if (player.Members[player.SelectedMember].currentHp <= 0 && !lostCorutineRunning)
        {
            StartCoroutine(Lost());
            lostCorutineRunning = true;
            return true;
        }
        return false;
    }
    public int Damage(TeamObject defender, TeamObject attacker, SkillObject skill)
    {
        float finalDamage = 0;
        finalDamage = (skill.damage + attacker.Members[attacker.SelectedMember].damage.ModifiedValue) * (float)(100.0f / (100.0f + defender.Members[defender.SelectedMember].defence.ModifiedValue));
        defender.Members[defender.SelectedMember].currentHp -= (int)finalDamage;
        if (defender.Members[defender.SelectedMember].currentHp < 0)
            defender.Members[defender.SelectedMember].currentHp = 0;
        return (int)finalDamage;
    }
    public int CalculateDamage(TeamObject defender, TeamObject attacker, SkillObject skill)
    {
        float finalDamage = 0;
        finalDamage = (skill.damage + attacker.Members[attacker.SelectedMember].damage.ModifiedValue) * (float)(100.0f / (100.0f + defender.Members[defender.SelectedMember].defence.ModifiedValue));
        return (int)finalDamage;
    }
    IEnumerator PlayerTurn(int skill)
    {
        if (player.Members[player.SelectedMember].skills[skill] == null)
        {
            chat.InitiateNextText("No skill set!");
            yield return new WaitForEndOfFrame();
        }
        else
        {
            bool canContinue = true;
            switch (skill)
            {
                case 0:
                    if (player.Members[player.SelectedMember].equipment.weapon.currentPP <= 0)
                        canContinue = false;
                    else
                        player.Members[player.SelectedMember].equipment.weapon.currentPP -= player.Members[player.SelectedMember].equipment.weapon.type.ppCost;
                    break;
                case 1:
                    if (player.Members[player.SelectedMember].equipment.armor.currentPP <= 0)
                        canContinue = false;
                    else
                        player.Members[player.SelectedMember].equipment.armor.currentPP -= player.Members[player.SelectedMember].equipment.armor.type.ppCost;
                    break;
                case 2:
                    if (player.Members[player.SelectedMember].equipment.shield.currentPP <= 0)
                        canContinue = false;
                    else
                        player.Members[player.SelectedMember].equipment.shield.currentPP -= player.Members[player.SelectedMember].equipment.shield.type.ppCost;
                    break;
                case 3:
                    if (player.Members[player.SelectedMember].equipment.trinket.currentPP <= 0)
                        canContinue = false;
                    else
                        player.Members[player.SelectedMember].equipment.trinket.currentPP -= player.Members[player.SelectedMember].equipment.trinket.type.ppCost;
                    break;
                default:
                    break;
            }
            if (canContinue)
            {
                SkillObject selectedSkill = player.Members[player.SelectedMember].skills[skill];
                int damage = CalculateDamage(enemy, player, selectedSkill);
                chat.InitiateNextText(string.Concat("You used ", selectedSkill.name, " and did ", damage, " damage!"));
                playerTurn = false;
                yield return new WaitForSeconds(1f);
                Damage(enemy, player, selectedSkill);
                SpawnSkill(selectedSkill.vfx, new Vector3(3.04f, 2.15f, 0), new Vector3(1, 1, 1));
                yield return new WaitForSeconds(1.5f);
                returnToOption.Raise();
                playerCorutineRunning = false;
                if (!CheckBattleWon())
                {
                    EndPlayerTurn();
                }
            }
            else
            {
                chat.InitiateNextText("Item is broken!");
                yield return new WaitForEndOfFrame();
            }
        }
        playerCorutineRunning = false;
    }
    public void SpawnSkill(GameObject vfx, Vector3 transform, Vector3 scale)
    {
        var obj = Instantiate(vfx, transform, Quaternion.identity);
        obj.transform.localScale = scale;
        obj.transform.GetChild(0).localScale = scale;
    }
    IEnumerator EnemyTurn()
    {
        explinationUI.color = new Color(1, 0.8f, 0.8f);
        chat.InitiateNextText("Selecting attack..");
        turnLabel.text = "Enemy Turn:";
        yield return new WaitForSeconds(1f);
        int radomSkill = Random.Range((int)0, (int)enemy.Members[enemy.SelectedMember].skills.Length);
        SkillObject skillToUse = null;
        while (skillToUse == null)
        {
            for (int i = 0; i < enemy.Members[enemy.SelectedMember].skills.Length; i++)
            {
                if (Random.Range(0, 2) > 0.5f)
                {
                    skillToUse = enemy.Members[enemy.SelectedMember].skills[i];
                }
            }
        }
        SkillObject skill = skillToUse;
        int damage = CalculateDamage(player, enemy, skill);
        chat.InitiateNextText(string.Concat("Enemy used ", skill.name, " and did ", damage, " damage!"));
        yield return new WaitForSeconds(1f);
        Damage(player, enemy, skill);
        SpawnSkill(skill.vfx, new Vector3(-3f, -0.5f, 0), skill.scale);

        yield return new WaitForSeconds(1.5f);
        if (!CheckBattleLost())
        {
            playerTurn = true;
            enemyCorutineRunning = false;
            explinationUI.color = new Color(1, 1, 1);
            turnLabel.text = "Player Turn:";
            chat.InitiateNextText(string.Concat("Select an option..."));
        }
    }
    IEnumerator Lost()
    {
        chat.InitiateNextText(string.Concat("Oh no!"));
        yield return new WaitForSeconds(1f);
        chat.InitiateNextText(string.Concat("You died..."));
        yield return new WaitForSeconds(2f);
        if (!AllCharactersDead(player))
        {
            playerTurn = true;
            enemyCorutineRunning = false;
            select_character_option.Raise();
        }
        else
        {
            player.pause = false;
            SceneSystem.LoadLastWorld();
        }
        lostCorutineRunning = false;
    }
    public bool AllCharactersDead(TeamObject team)
    {
        for (int i = 0; i < team.Members.Count; i++)
        {
            if (team.Members[i].currentHp > 0)
            {
                return false;
            }
        }
        return true;
    }
}
