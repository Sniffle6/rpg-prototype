﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Entity", menuName = "Entity/New Entity")]
public class EntityObject : ScriptableObject
{
    public Sprite EntityDisplay;
    //public string Name;
    public BaseStat hp;
    public BaseStat damage;
    public BaseStat defence;
    public RuntimeAnimatorController controller;
    //public SkillObject[] Skills;
}
[System.Serializable]
public class BaseStat
{
    public int stat;
    public float growth;
}