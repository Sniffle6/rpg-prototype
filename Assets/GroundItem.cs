﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundItem : MonoBehaviour
{
    public Items item;
    public InventoryObject inventory;
    public ChatSystem chat;
    public GameObject chatPanel;
    bool canPickUp = false;
    bool corutineRunning;
    bool pickedUp = false;
    public Sprite open;
    public Sprite closed;
    private void Start()
    {
        //GetComponentInChildren<SpriteRenderer>().sprite = item.type.display;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && !pickedUp)
        {
            canPickUp = true;
            chatPanel.SetActive(true);
            chat.InitiateNextText(string.Concat("Press space to pick up '", item.type.name, "'."));
            GetComponentInChildren<SpriteRenderer>().sprite = open;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.tag == "Player" && !pickedUp)
        {
            canPickUp = false;
            chat.InitiateNextText("");
            chatPanel.SetActive(false);
            GetComponentInChildren<SpriteRenderer>().sprite = closed;
        }
    }
    private void Update()
    {
        if (canPickUp)
        {
            if (Input.GetKeyDown(KeyCode.Space) && !corutineRunning)
            {
                StartCoroutine(PickUp());
                corutineRunning = true;
            }
        }
    }
    IEnumerator PickUp()
    {
        inventory.items.Add(item);
        chat.InitiateNextText(string.Concat("Picked up a '", item.type.name, "'."));
        GetComponentInChildren<SpriteRenderer>().sprite = null;
        canPickUp = false;
        pickedUp = true;
        yield return new WaitForSeconds(2f);
        chat.InitiateNextText("");
        chatPanel.SetActive(false);
        corutineRunning = false;

    }
}
